const assert = require('assert');
const {url} = require('url');
const path = require('path');

describe('Hedonist page', () => {
    

    xit('should have been login with invalid data', () => {
        browser.url('https://staging.bsa-hedonist.online/login');

        
        const emailField = $('input[name=email]');
        const passwordField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');

        emailField.setValue('dark@i.fr');
        passwordField.setValue('paris');
        loginButton.click();

        $("div.toast.is-danger.is-top:first-child").waitForExist(5000);
        const aaa = $("div.toast.is-danger.is-top:first-child");
        assert.equal(aaa.getText(), 'The email or password is incorrect')
    });

    xit('should have the registration', () => {
        browser.url('https://staging.bsa-hedonist.online/signup');
        const emailValue = "dark.janna@i.fr";
        const passwordValue = "parisparis";
        const firstNameField = $('input[name=firstName]');
        const lastNameField = $('input[name=lastName]');
        const emailField = $('input[name=email]');
        const passwordField = $('input[type=password]');
        const singupButton = $('button.button.is-primary');
        const loginButton = $('button.button.is-primary');

        firstNameField.setValue('Janna');
        lastNameField.setValue('Dark');
        emailField.setValue(emailValue);
        passwordField.setValue(passwordValue);
        singupButton.click();

        browser.pause(3000);
        {
            const url = new URL(browser.getUrl());
            const actualUrl = url.hostname.toString() + url.pathname.toString();
            assert.equal(actualUrl, 'staging.bsa-hedonist.online/login');
        }


        emailField.setValue(emailValue);
        passwordField.setValue(passwordValue);
        loginButton.click();

        browser.pause(1000);

        const url = new URL(browser.getUrl());
        const actualUrl = url.hostname.toString() + url.pathname.toString();

        assert.equal(actualUrl, 'staging.bsa-hedonist.online/search');
    });

     xit('should have been login with valid data', () => {
        browser.url('https://staging.bsa-hedonist.online/login');

        
        const emailField = $('input[name=email]');
        const passwordField = $('input[type=password]');
        const loginButton = $('button.button.is-primary');

        emailField.setValue('15dark@i.fr');
        passwordField.setValue('parisparis');
        loginButton.click();
        browser.pause(2000);
        
    });

      
      xit('should have created a new list', () => {
        browser.url('https://staging.bsa-hedonist.online/my-lists');
        name = "My favourite";
        $('a.button.is-success').waitForDisplayed(5000);
         browser.execute(() => {
              document.querySelectorAll('a[href="/my-lists/add"]')[0].click();
        });
        const listName = $('input[id=list-name]');
              listName.setValue(name);
        const saveButton = $('button.button.is-success');
              saveButton.click();
         
        
        $("div.toast.is-success.is-top:first-child").waitForExist(5000);
        const aaa = $("div.toast.is-success.is-top:first-child");
     
        assert.equal(aaa.getText(), "The list was saved!");
    });


    xit('should have deleted a list', () => {
        browser.url('https://staging.bsa-hedonist.online/my-lists');
        browser.pause(3000);
        
       
        const deleteListButton = $$("button.button.is-danger")[0];
        browser.pause(3000);
        deleteListButton.click();
     
      const vdf = $("div.buttons.is-centered").$('button.button.is-danger');
            vdf.click();

        
        $("div.toast.is-info.is-top:first-child").waitForExist(5000);
        const ggg = $("div.toast.is-info.is-top:first-child");
        assert.equal(ggg.getText(), "The list was removed");
        
    });

     xit('should have created a new place', () => {
        browser.maximizeWindow();
        browser.url('https://staging.bsa-hedonist.online/places/add');

        const placename = $("input.input.is-medium");
              placename.setValue("Celentano");
              
      /*  const location = $('input[placeholder="Location"]')[1];
              location.clearValue();
              location.setValue('Lviv');*/
        const zipcodefield = $("input[placeholder='09678']");
              zipcodefield.setValue("79000");
        const addressfield = $("input[placeholder^=Khreschatyk");
              addressfield.setValue("Svobody st., 20");
        const phonefield = $("input.input[type=tel]");
              phonefield.setValue("+380671234567");
        const websitefield = $('input[placeholder="http://the-best-place.com/"]');
              websitefield.setValue("https://cafe.com");
        const descriptionfield = $("textarea");
              descriptionfield.setValue("The Best pizza in your life.");
        const nextbuttons = $$("span.button.is-success");
              nextbuttons[0].click();
        
      const uploadControl = $('input[type=file]');
            uploadControl.setValue("D:\\QA\\bsa19\\bs-academy-2019-parfeniuk_anastasiia\\image.jpg");
     
              nextbuttons[1].click();
              browser.pause(1000);
              nextbuttons[2].click();
              browser.pause(1000);
        const category = $$("select")[0].selectByIndex(3);
        browser.pause(1000);
        const tags = $$("select")[1].selectByIndex(1);
              browser.pause(1000);
              nextbuttons[3].click();
              browser.pause(5000);
        $$("span.check.is-success")[1].click();
        browser.pause(1000);
         nextbuttons[4].click();
         browser.pause(1000);
         nextbuttons[5].click();
         browser.pause(3000);
         nextbuttons[6].click();
   
        browser.pause(5000);
        const url = new URL(browser.getUrl());
        const actualUrl = url.pathname.toString();
        assert(actualUrl.startsWith("/places"));
    });
    
});