const fetch = require('node-fetch');
{
    const url = 'http://165.227.137.250/api/v1'
    const args = require('./testData2.json');

    class Client {

        static async loginRequest(url, args) {
            return fetch(`${url}/auth/login`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: args.email,
                    password: args.password
                })
            });
        };

        static async sendAddListRequest(url, args, token) {
            return fetch(`${url}/user-lists/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}` 
                },
                body: JSON.stringify({
                    user_id: args.user_id,
                    name: args.name,
                    updated_at: args.updated,
                    created_at: args.created,
                    id: args.list_id
                })
            });
        };
    };

    async function addList(url, args) {

        console.log(`Login ${url}`);
        const authresponse = await Client.loginRequest(url, args);

        if (authresponse.status !== 200) {
            console.log(`Login failed on ${url}`);
            const responseJSON = await authresponse.json();
            const error = new Error(`Failed to login on ${url}`);
            error.message = '' + JSON.stringify(responseJSON.error);
            return Promise.reject(error);
        }
        console.log(`Add list ${url}`);
        const json = await authresponse.json();
        console.log(`Token ${json.data.access_token}`)
        const response = await Client.sendAddListRequest(url, args, json.data.access_token);

        if (response.status === 201) {
            console.log(`List is added on ${url}`);
            return Promise.resolve();
        }
        const responseJSON = await response.json();
        const error = new Error(`Failed to successfully adding list for ${url}`);
        error.message = '' + JSON.stringify(responseJSON.error);
        return Promise.reject(error);
        
    }

    function handleError(error) {
        const errorBody = () => {
            return error && error.message ? error.message : error;
        };
        console.log('Error during bootstrap, exiting:', errorBody());
        process.exit(1);

    };

    module.exports = (async done => {
        console.log('========Start=========');
        console.log('========Adding list=========');
        addList(url, args)
            .then(() => {})
            .catch(error => {
                done(handleError(error));
            })
    });
}
