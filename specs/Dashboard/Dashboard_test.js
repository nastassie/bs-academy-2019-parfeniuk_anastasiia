const credentials = require('./../testData.json');
const Help = require('../../helpers/helpers');
const Assert = require('../../helpers/validators');
const Wait = require('../../helpers/waiters');
//const LoginPage = require('../Login/page/Login_po');
//const page = new LoginPage();

const validate = new Assert();
const wait = new Wait();
const assert = require('chai').assert;
describe('Online-IDE', () => {
    
    beforeEach(() => {
       browser.maximizeWindow();
       browser.url(credentials.appUrl);
       browser.pause(5000);
      //browser.url('http://bsa-ide.azurewebsites.net')
       Help.loginWithDefaultUser();
       //Wait.forSpinner();
    
   });

   afterEach(() => {
       browser.reloadSession();
   });

   it('delete a project', () => {
       
      browser.pause(5000);
       Help.clickprojectItemOnDashboard(credentials.projectNameDelete);
       validate.navigationToPage(credentials.projectDetailsUrl);
      // validate.successNotificationTextIs(credentials.notificationRegistrationSuccess);
      // wait.forNotificationToDisappear(); 
      // browser.pause(5000); 
       Help.logOut();
       
   });

   
          
   
});